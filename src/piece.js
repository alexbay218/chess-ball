const { Sleeping, Body, Bodies, Composite } = require('matter-js');
const PIXI = require('pixi.js');

const defaultCollision = 0x0001;
const whiteCollision = 0x0002;
const blackCollision = 0x0004;
class Piece {
  constructor(global, state) {
    this.global = global;

    //Internal state
    this.state = state;
    this.type = null;
    this.isMoving = false;
    this.isCapturing = false;
    this.isCaptured = false;
    this.hasCaptured = false;
    this.drawingPlayer = '';

    //Engine body
    var collision = {
      mask: defaultCollision | whiteCollision | blackCollision,
      category: this.state.player === 'white' ? whiteCollision : blackCollision
    };
    if(this.state.type === 'knight') {
      if(this.state.player === 'white') {
        collision = {
          mask: defaultCollision | blackCollision,
          category: whiteCollision
        };
      }
      else {
        collision = {
          mask: defaultCollision | whiteCollision,
          category: blackCollision
        };
      }
    }
    this.body = Bodies.circle(this.state.x, this.state.y, 45, { collisionFilter: collision });
    this.body.frictionAir = 0.035;
    this.body.restitution = 1;
    Composite.add(this.global.engine.world, this.body);

    //PIXI display
    this.graphics = new PIXI.Graphics();
    this.overlayGraphics = new PIXI.Graphics();
    this.overlayGraphics.alpha = 0.4;
    this.sprite = null;
    this.container = new PIXI.Container();
    this.global.pieceLayer.addChild(this.container);
    this.container.addChild(this.graphics);
    this.global.overlayLayer.addChild(this.overlayGraphics);

    this.interact();
    this.update();
    this.engineListener = this.global.emitter.on('engineUpdate', this.engineUpdate.bind(this));
    this.engineDoneListener = this.global.emitter.on('engineDone', this.engineDone.bind(this));
    this.netReceivedListener = this.global.emitter.on('netReceived', this.netReceived.bind(this));
  }
  engineUpdate() {
    this.state.x = this.body.position.x;
    this.state.y = this.body.position.y;
    this.update();
  }
  engineDone() {
    this.isCapturing = false;
    this.isMoving = false;
    this.update();
  }
  update() {
    if(this.drawingPlayer !== this.global.state.player) {
      this.drawingPlayer = this.global.state.player;
    }

    //Real X / Y
    var x = this.state.x + 100;
    var y = this.state.y + 100;
    if(this.drawingPlayer === 'black') {
      x = 900 - this.state.x;
      y = 900 - this.state.y;
    }
    var scaleW = 70;
    var scaleH = 70;

    //Piece specific offsets
    var offsetX = 0;
    var offsetY = -3;
    if(this.state.type === 'pawn') {
      offsetY = -13;
      scaleW = 90;
      scaleH = 90;
    }
    if(this.drawingPlayer === 'black' && false) {
      offsetX = -offsetX;
      offsetY = -offsetY;
    }

    if(this.sprite === null || this.type !== this.state.type) {
      this.type = this.state.type;
      if(this.state.type === 'pawn') {
        Body.setDensity(this.body, 0.2/Math.sqrt(0.38));
      }
      else if(this.state.type === 'knight') {
        Body.setDensity(this.body, 0.01/Math.sqrt(0.84));
      }
      else if(this.state.type === 'king') {
        Body.setDensity(this.body, 0.01/Math.sqrt(0.38));
      }
      else if(this.state.type === 'rook') {
        Body.setDensity(this.body, 0.01/Math.sqrt(2.545));
      }
      else {
        Body.setDensity(this.body, 0.01/Math.sqrt(3.6));
      }
      if(this.sprite !== null) { this.sprite.destroy(); }
      var texture = PIXI.Texture.from(`assets/pieces/${this.state.player}_${this.state.type}.png`);
      this.sprite = new PIXI.Sprite(texture);
      this.sprite.width = scaleW;
      this.sprite.height = scaleH;
      this.sprite.anchor.set(0.5);
      this.container.addChild(this.sprite);
    }
    this.sprite.x = x + offsetX;
    this.sprite.y = y + offsetY;

    this.graphics.clear();

    if(this.isCaptured) {
      this.graphics.beginFill(0xff0000);
    }
    else if(this.isCapturing) {
      this.graphics.beginFill(0x00aa00);
    }
    else {
      this.graphics.beginFill(this.state.player === 'white' ? 0x000000 : 0xffffff);
    }
    this.graphics.drawCircle(x, y, 45);
    this.graphics.beginFill(this.state.player === 'white' ? 0xffffff : 0x000000);
    this.graphics.drawCircle(x, y, 40);
    this.graphics.endFill();
  }
  overlayType() {
    this.overlayGraphics.beginFill(0xaa0000);
    var r = 100;
    if(this.state.type === 'rook') {
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, Math.PI/36, (Math.PI*71)/36, true);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, Math.PI/36 + (Math.PI)/2, (Math.PI*71)/36 + (Math.PI)/2, true);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, Math.PI/36 + (Math.PI*2)/2, (Math.PI*71)/36 + (Math.PI*2)/2, true);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, Math.PI/36 + (Math.PI*3)/2, (Math.PI*71)/36 + (Math.PI*3)/2, true);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
    }
    if(this.state.type === 'knight') {
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, Math.atan(1/2) - Math.PI/36, Math.atan(1/2) + Math.PI/36, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, Math.atan(2) - Math.PI/36, Math.atan(2) + Math.PI/36, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.atan(1/2) - Math.PI/36) + (Math.PI)/2, (Math.atan(1/2) + Math.PI/36) + (Math.PI)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.atan(2) - Math.PI/36) + (Math.PI)/2, (Math.atan(2) + Math.PI/36) + (Math.PI)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.atan(1/2) - Math.PI/36) + (Math.PI*2)/2, (Math.atan(1/2) + Math.PI/36) + (Math.PI*2)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.atan(2) - Math.PI/36) + (Math.PI*2)/2, (Math.atan(2) + Math.PI/36) + (Math.PI*2)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.atan(1/2) - Math.PI/36) + (Math.PI*3)/2, (Math.atan(1/2) + Math.PI/36) + (Math.PI*3)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.atan(2) - Math.PI/36) + (Math.PI*3)/2, (Math.atan(2) + Math.PI/36) + (Math.PI*3)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
    }
    if(this.state.type === 'bishop') {
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.PI*2)/9, (Math.PI*5)/18, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.PI*2)/9 + (Math.PI)/2, (Math.PI*5)/18 + (Math.PI)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.PI*2)/9 + (Math.PI*2)/2, (Math.PI*5)/18 + (Math.PI*2)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.PI*2)/9 + (Math.PI*3)/2, (Math.PI*5)/18 + (Math.PI*3)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
    }
    if(this.state.type === 'pawn') {
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.PI*2)/9, (Math.PI*5)/18, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, Math.PI/36 + (Math.PI)/2, (Math.PI*71)/36 + (Math.PI)/2, true);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.arc(this.sprite.x, this.sprite.y, r, (Math.PI*2)/9 + (Math.PI)/2, (Math.PI*5)/18 + (Math.PI)/2, false);
      this.overlayGraphics.lineTo(this.sprite.x, this.sprite.y);
    }
    if(this.state.type === 'king' || this.state.type === 'queen') {
      this.overlayGraphics.drawCircle(this.sprite.x, this.sprite.y, r);
    }
    this.overlayGraphics.endFill();
  }
  pointerData(x,y) {
    var mag = Math.sqrt(Math.pow(Math.abs(this.sprite.x - x), 2) + Math.pow(Math.abs(this.sprite.y - y), 2));
    var ang = 2 * Math.atan((y - this.sprite.y)/((x - this.sprite.x) + mag));
    if(mag < 10) { mag = 10; }
    if(mag > 100) { mag = 100; }
    var deg = ang * (180 / Math.PI);
    if(this.state.type === 'rook') {
      if(deg > 5 && deg < 45) { deg = 5; }
      if(deg >= 45 && deg < 85) { deg = 85; }
      if(deg > 95 && deg < 135) { deg = 95; }
      if(deg >= 135 && deg < 175) { deg = 175; }
      if(deg < -5 && deg > -45) { deg = -5; }
      if(deg <= -45 && deg > -85) { deg = -85; }
      if(deg < -95 && deg > -135) { deg = -95; }
      if(deg <= -135 && deg > -175) { deg = -175; }
    }
    if(this.state.type === 'knight') {
      if(deg >= 0 && deg < 21.5651) { deg = 21.5651; }
      if(deg > 31.5651 && deg < 45) { deg = 31.5651; }
      if(deg >= 45 && deg < 58.4349) { deg = 58.4349; }
      if(deg > 68.4349 && deg < 90) { deg = 68.4349; }
      if(deg >= 90 && deg < 111.56519) { deg = 111.56519; }
      if(deg > 121.56519 && deg < 135) { deg = 121.56519; }
      if(deg >= 135 && deg < 148.4349) { deg = 148.4349; }
      if(deg > 158.4349) { deg = 158.4349; }
      if(deg <= 0 && deg > -21.5651) { deg = -21.5651; }
      if(deg < -31.5651 && deg > -45) { deg = -31.5651; }
      if(deg <= -45 && deg > -58.4349) { deg = -58.4349; }
      if(deg < -68.4349 && deg > -90) { deg = -68.4349; }
      if(deg <= -90 && deg > -111.56519) { deg = -111.56519; }
      if(deg < -121.56519 && deg > -135) { deg = -121.56519; }
      if(deg <= -135 && deg > -148.4349) { deg = -148.4349; }
      if(deg < -158.4349) { deg = -158.4349; }
    }
    if(this.state.type === 'bishop') {
      if(deg >= 0 && deg < 40) { deg = 40; }
      if(deg > 50 && deg < 90) { deg = 50; }
      if(deg >= 90 && deg < 130) { deg = 130; }
      if(deg > 140) { deg = 140; }
      if(deg <= 0 && deg > -40) { deg = -40; }
      if(deg < -50 && deg > -90) { deg = -50; }
      if(deg <= -90 && deg > -130) { deg = -130; }
      if(deg < -140) { deg = -140; }
    }
    if(this.state.type === 'pawn') {
      if(deg >= 0 && deg < 40) { deg = 40; }
      if(deg > 50 && deg < 67.5) { deg = 50; }
      if(deg >= 67.5 && deg < 85) { deg = 85; }
      if(deg > 95 && deg < 107.5) { deg = 95; }
      if(deg >= 107.5 && deg < 130) { deg = 130; }
      if(deg > 140) { deg = 140; }
      if(deg <=0) { deg = 40; }
    }
    ang = deg * (Math.PI / 180);
    return {
      x: this.sprite.x + (mag * Math.cos(ang)),
      y: this.sprite.y + (mag * Math.sin(ang)),
    };
  }
  velocityData(x,y) {
    var sourcePoint = this.pointerData(x,y);
    var mag = Math.sqrt(Math.pow(Math.abs(this.sprite.x - sourcePoint.x), 2) + Math.pow(Math.abs(this.sprite.y - sourcePoint.y), 2));
    var ang = 2 * Math.atan((sourcePoint.y - this.sprite.y)/((sourcePoint.x - this.sprite.x) + mag));
    var deg = ang * (180 / Math.PI);
    if(this.state.type === 'knight') {
      return {
        x: ((sourcePoint.x - this.sprite.x) * -0.84) + this.sprite.x,
        y: ((sourcePoint.y - this.sprite.y) * -0.84) + this.sprite.y
      };
    }
    if(this.state.type === 'king') {
      return {
        x: ((sourcePoint.x - this.sprite.x) * -0.38) + this.sprite.x,
        y: ((sourcePoint.y - this.sprite.y) * -0.38) + this.sprite.y
      };
    }
    if(this.state.type === 'pawn') {
      if(!this.state.hasMoved && deg >= 84 && deg <= 96) {
        return {
          x: ((sourcePoint.x - this.sprite.x) * -0.72) + this.sprite.x,
          y: ((sourcePoint.y - this.sprite.y) * -0.72) + this.sprite.y
        };
      }
      return {
        x: ((sourcePoint.x - this.sprite.x) * -0.38) + this.sprite.x,
        y: ((sourcePoint.y - this.sprite.y) * -0.38) + this.sprite.y
      };
    }
    if(this.state.type === 'rook') {
      return {
        x: ((sourcePoint.x - this.sprite.x) * -2.545) + this.sprite.x,
        y: ((sourcePoint.y - this.sprite.y) * -2.545) + this.sprite.y
      };
    }
    return {
      x: ((sourcePoint.x - this.sprite.x) * -3.6) + this.sprite.x,
      y: ((sourcePoint.y - this.sprite.y) * -3.6) + this.sprite.y
    };
  }
  canCapture(x,y) {
    var mag = Math.sqrt(Math.pow(Math.abs(this.sprite.x - x), 2) + Math.pow(Math.abs(this.sprite.y - y), 2));
    var ang = 2 * Math.atan((y - this.sprite.y)/((x - this.sprite.x) + mag));
    if(mag < 10) { mag = 10; }
    if(mag > 100) { mag = 100; }
    var deg = ang * (180 / Math.PI);
    if(this.state.type === 'pawn') {
      if(deg > 80 && deg < 100) { return false; }
      if(deg < -80 && deg > -100) { return false; }
    }
    return true;
  }
  moveStart(event) {
    this.overlayGraphics.clear();
    if(!this.isMoving && !this.global.state.isActive && this.global.state.player === this.state.player && this.global.state.canPlay) {
      this.isMoving = true;
      this.overlayType();
    }
  }
  moveUpdate(event) {
    this.overlayGraphics.clear();
    if(this.isMoving && !this.global.state.isActive && this.global.state.player === this.state.player && this.global.state.canPlay) {
      var sourcePoint = this.global.viewport.toWorld(event.data.global);
      var targetPoint = this.pointerData(sourcePoint.x,sourcePoint.y);
      var targetVelocityPoint = this.velocityData(sourcePoint.x,sourcePoint.y);
      var canCapture = this.canCapture(targetPoint.x, targetPoint.y);
      this.overlayType();
      this.overlayGraphics.lineStyle({ width: 10, color: 0x00aa00 });
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.lineTo(targetPoint.x, targetPoint.y);
      this.overlayGraphics.lineStyle({ width: 10, color: canCapture ? 0x0000aa : 0x00aaaa });
      this.overlayGraphics.moveTo(this.sprite.x, this.sprite.y);
      this.overlayGraphics.lineTo(targetVelocityPoint.x, targetVelocityPoint.y);
      this.overlayGraphics.closePath();
    }
  }
  moveEnd(event) {
    this.overlayGraphics.clear();
    if(this.isMoving && !this.global.state.isActive && this.global.state.player === this.state.player && this.global.state.canPlay) {
      var sourcePoint = this.global.viewport.toWorld(event.data.global);
      var targetPoint = this.pointerData(sourcePoint.x,sourcePoint.y);
      var targetVelocityPoint = this.velocityData(sourcePoint.x,sourcePoint.y);
      var canCapture = this.canCapture(targetPoint.x, targetPoint.y);
      this.isCapturing = canCapture;
      if(this.drawingPlayer === 'white') {
        this.move(
          (targetVelocityPoint.x - this.sprite.x) / 10,
          (targetVelocityPoint.y - this.sprite.y) / 10,
        );
      }
      else {
        this.move(
          (this.sprite.x - targetVelocityPoint.x) / 10,
          (this.sprite.y - targetVelocityPoint.y) / 10,
        );
      }
    }
    this.isMoving = false;
  }
  interact() {
    this.graphics.interactive = true;
    this.graphics.on('pointerdown', (event) => {
      this.moveStart(event);
    });
    this.graphics.on('pointermove', (event) => {
      this.moveUpdate(event);
    });
    this.graphics.on('pointerup', (event) => {
      this.moveEnd(event);
    });
    this.graphics.on('pointerupoutside', (event) => {
      this.moveEnd(event);
    });
  }
  async netReceived(data) {
    if(data.type === 'move' && data.data.id === this.state.id) {
      this.move(data.data.x, data.data.y, false);
    }
    if(data.type === 'state') {
      if(this.global.state.isActive) {
        await (new Promise((r) => {
          var e = this.global.emitter.on('engineDone', () => {
            e();
            r();
          });
        }));
      }
      var pieces = data.data.pieces;
      for(var i = 0;i < pieces.length;i++) {
        if(pieces[i].id === this.state.id) {
          Body.setPosition(this.body, { x: pieces[i].x, y: pieces[i].y });
          this.engineUpdate();
        }
      }
    }
  }
  move(vx, vy, emitNet = true) {
    this.global.state.isActive = true;
    this.state.hasMoved = true;
    var canCapture = this.canCapture(this.sprite.x + (vx * 10), this.sprite.y + (vy * 10));
    this.isCapturing = canCapture;
    Body.setVelocity(this.body, { x: vx, y: vy });
    Sleeping.set(this.body, false);
    this.global.emitter.emit('engineStart');
    if(emitNet && this.global.state.type == 'network') {
      this.global.emitter.emit('netSend', {
        type: 'move',
        data: { id: this.state.id, x: vx, y: vy }
      });
    }
  }
  destroy() {
    this.engineListener();
    this.engineDoneListener();
    this.netReceivedListener();
    Composite.remove(this.global.engine.world, this.body);
    this.container.destroy();
  }
};

module.exports = Piece;
