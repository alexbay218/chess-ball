const WebSocket = require('isomorphic-ws');
const copy = require('copy-to-clipboard');

class Network {
  constructor(global) {
    this.global = global;

    this.ws = null;
    this.type = null;
    this.id = null;

    this.urlSearch = new URLSearchParams(window.location.search);
    if(this.urlSearch.has('type')) {
      this.global.state.type = 'network';
      this.global.state.canPlay = false;
      this.type = this.urlSearch.get('type');
      this.global.state.canPlay = this.type === 'host';
      this.ws = new WebSocket('ws://35.202.152.170:2122');
      this.initConnection();
    } else {
      this.global.state.type = 'local';
    }

    this.netSendListener = this.global.emitter.on('netSend', (data) => {
      if(typeof data.id !== 'string') {
        data.id = this.id;
      }
      if(typeof data.id === 'string') {
        this.ws.send(JSON.stringify(data));
      }
    });
  }
  initConnection() {
    this.ws.onmessage = async (msgObj) => {
      var msg = JSON.parse(msgObj.data);
      if(msg.type === 'connect') {
        alert('Opponent Connected!');
      }
      if(msg.type === 'close') {
        alert('Opponent Disconnected!');
        this.ws.close();
      }
      if(msg.type === 'self') {
        this.id = msg.data;
        if(this.type !== 'host' && this.urlSearch.has('host')) {
          this.ws.send(JSON.stringify({ type: 'host', data: this.urlSearch.get('host'), id: this.id }));
        }
        else {
          copy(`${window.location.origin}${window.location.pathname}?type=client&host=${this.id}`);
        }
      }
      else {
        if(msg.type === 'move') {
          this.global.hasMoveQueued = true;
        }
        if(this.global.state.isActive) {
          await (new Promise((r) => {
            var e = this.global.emitter.on('engineDone', () => {
              e();
              r();
            });
          }));
        }
        this.global.emitter.emit('netReceived', msg);
      }
    };
  }
}

module.exports = Network;
